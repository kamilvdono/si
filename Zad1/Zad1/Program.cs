﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Zad1.Model;
using Zad1.Utils;

namespace Zad1
{
	class Program
	{
		//private static readonly int[] POPULATION_SIZES = {50, 250, 500, 1200};
		private static readonly int[] POPULATION_SIZES = {800};
		//private static readonly float[] CROSS_PROBABILITIES = {0f, 0.1f, 0.5f, 0.7f, 1f};
		private static readonly float[] CROSS_PROBABILITIES = {0.01f};
		//private static readonly int[] MAX_GENERATIONS = {100, 200, 500, 800};
		private static readonly int[] MAX_GENERATIONS = {100};
		//private static int[] TOURS = {10, 15, 25};
		private static int[] TOURS = { 5 };
		//private static float[] MUTATION_PROBABILITIES = {0, 0.002f, 0.01f, 0.05f, 0.2f, 0.8f, 1f};
		private static float[] MUTATION_PROBABILITIES = { 0.2f};
		//private static Configuration.CrossType[] Crossings = { Configuration.CrossType.TourRandom, Configuration.CrossType.Random, Configuration.CrossType.Tour };
		private static Configuration.CrossType[] Crossings = {Configuration.CrossType.Tour};

		//private static float MAX_SCORE = 5000000;

		private static string[] DATA_FILES;/* =
		{
			"easy_0.ttp",
			"easy_1.ttp",
			"easy_2.ttp",
			"easy_3.ttp",
			"easy_4.ttp",
			"hard_0.ttp",
			"hard_1.ttp",
			"hard_2.ttp",
			"hard_3.ttp",
			"hard_4.ttp",
			"medium_0.ttp",
			"medium_1.ttp",
			"medium_2.ttp",
			"medium_3.ttp",
			"medium_4.ttp",
			"mine_0.ttp",
			"trivial_0.ttp",
			"trivial_1.ttp"
		};*/

		private static string OUTPUT_PATH = "scores.csv";

		private static Configuration LoadedConfiguration;

		static void Main(string[] args)
		{
			Console.WriteLine("Starting");
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

			if (System.IO.Directory.Exists("Out"))
			{
				System.IO.Directory.Move("Out", $"Out_{DateTime.Now.Ticks}");
			}

			DATA_FILES = System.IO.Directory.GetFiles("Data").Select(d => d.Split('\\')[1].Split('.')[0]).ToArray()
				.Where(d => d.Contains("2"))
				.Where(d => d.Contains("medium"))
				.ToArray();

			List<Environment> environments = new List<Environment>();

			foreach (var dataFile in DATA_FILES)
			{
				foreach (Configuration.CrossType crossType in Crossings)
				{
					foreach (var mutationProbability in MUTATION_PROBABILITIES)
					{
						foreach (var tour in TOURS)
						{
							foreach (var maxGenerations in MAX_GENERATIONS)
							{
								foreach (var crossProbability in CROSS_PROBABILITIES)
								{
									foreach (var populationSize in POPULATION_SIZES)
									{
										for (int z = 0; z < 10; z++)
										{
											environments.Add(new Environment(dataFile, populationSize, crossProbability, maxGenerations, tour, mutationProbability, crossType));
										}
									}
								}

								if (environments.Count > 32)
								{
									using (CountdownEvent counter = new CountdownEvent(environments.Count))
									{
										for (int i = 0; i < environments.Count; i++)
										{
											int index = i;
											ThreadPool.QueueUserWorkItem(_ => environments[index].Simulate(counter));
										}
										counter.Wait();
									}

									environments.Clear();
									Console.WriteLine("Processing logs");

									DataFileLoader.ProcessLogs();
								}
							}
						}
					}
				}
			}

			

			using (CountdownEvent counter = new CountdownEvent(environments.Count))
			{
				for (int i = 0; i < environments.Count; i++)
				{
					int index = i;
					ThreadPool.QueueUserWorkItem(_ => environments[index].Simulate(counter));
				}
				counter.Wait();
			}

			environments.Clear();

			Console.WriteLine("Processing logs");

			DataFileLoader.ProcessLogs();

			Console.WriteLine("Press enter to exit");
			Console.Read();
		}
	}

	class Environment
	{
		private static int CurrentID = 0;

		private const string DATA_PREFIX = "Data/";
		private readonly string DataPath;
		private readonly int PopulationSize;
		private readonly float CrossProbability;
		private readonly int MaxGenerations;
		private readonly int Tour;
		private readonly float MutationProbability;
		private readonly Configuration.CrossType CrossingType;

		private int ID;
		private Configuration Configuration;
		private Population Population;

		public Environment()
		{
			ID = CurrentID++;
		}

		public Environment(string dataPath, int populationSize, float crossProbability, int maxGenerations, int tour, float mutationProbability, Configuration.CrossType crossingType)
		{
			ID = CurrentID++;
			DataPath = dataPath;
			PopulationSize = populationSize;
			CrossProbability = crossProbability;
			MaxGenerations = maxGenerations;
			Tour = tour;
			MutationProbability = mutationProbability;
			CrossingType = crossingType;


			Setup();
		}

		public void Setup()
		{
			Console.WriteLine($"Start load data: {ID}");
			Configuration = Configuration.Load($"{DATA_PREFIX}{DataPath}.ttp");

			Console.WriteLine($"Data loaded {ID}");
			Configuration.MutationProbability = MutationProbability;

			Configuration.CrossingType = CrossingType;

			Console.WriteLine($"Setup population {ID}");
			Population =
				new Population(PopulationSize, CrossProbability, MaxGenerations, 0, Tour, Configuration);
			Population.IsDone = () => (Population.IsMaxGeneration());
		}

		public void Simulate(CountdownEvent evt)
		{
			Console.WriteLine($"Starting simulation {ID}");
			Population.Simulate();

			Console.WriteLine($"Write simulation data {ID}");

			StringBuilder sb = new StringBuilder("Nr pokolenia;Najlepsza ocena;Srednia ocena;Najgorsza ocena;Czas generacji;Plik;Rozmiar populacji;Prawdopodobienstwo rozmnozenia;Maksymalna ilosc generacji;Rozmiar turnieju;Prawdopodobienstwo mutacji\n");
			sb.Append(Population.Log);

			string fileName =
				$"{DataPath}__PS{PopulationSize}_PC{CrossProbability}_MG{MaxGenerations}_T{Tour}_PM{MutationProbability}_{CrossingType.ToString()}";

			string log = sb.ToString().Replace('.', ',');

			System.IO.Directory.CreateDirectory("Out");
			File.WriteAllText($"Out/{fileName}_ID{ID}_TS{DateTime.Now.Ticks}.csv", log);
			Console.WriteLine($"All done{ID}\nBest score is {Population.Thieves[0].Score}");
			evt?.Signal();
		}
	}
}
