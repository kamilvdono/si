﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1.Model
{
	public class Item
	{
		public int Index;
		public int Profit;
		public int Weight;
		public int CityIndex;

		public float ProfitRatio;
	}
}
