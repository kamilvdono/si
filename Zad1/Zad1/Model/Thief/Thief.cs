﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Zad1.Utils;

namespace Zad1.Model
{
	public class Thief
	{
		Random Random = new Random();

		public int[] ReproduceArray;

		public MinMax<float> Speed => Configuration.Speed;
		public Configuration.CrossType CrossingType => Configuration.CrossingType;
		public List<int> Cities;
		public List<int> Items;
		public List<float> CitySpeed;
		public Configuration Configuration;

		private float? _Score;
		public float Score => (_Score ?? (_Score = CalculateScore())).Value;

		private int? _KnapsackCapacity;
		private int KnapsackCapacity => (_KnapsackCapacity ?? (_KnapsackCapacity = Configuration.KnapsackCapacity)).Value;

		public Thief(int[] reproduceArray)
		{
			ReproduceArray = reproduceArray;
		}

		public static Thief GetRandom(Configuration configuration, int[] reproduceArray)
		{
			Thief thief = new Thief(reproduceArray)
			{
				Configuration = configuration,
			};

			thief.Cities = ShuffleList(Enumerable.Range(0, configuration.Cities.Count).ToList());
			thief.FillItems();

			return thief;
		}

		public void Copy(Thief other)
		{
			if (Cities != null && Cities.Count == other.Cities.Count)
			{
				for (int i = 0; i < other.Cities.Count; i++)
				{
					Cities[i] = other.Cities[i];
				}
			}
			else
			{
				Cities = new List<int>(other.Cities.Count);
				for (int i = 0; i < other.Cities.Count; i++)
				{
					Cities.Add(other.Cities[i]);
				}
			}

			if (Items != null && Items.Count == other.Items.Count)
			{
				for (int i = 0; i < other.Items.Count; i++)
				{
					Items[i] = other.Items[i];
				}
			}
			else
			{
				if (Items == null)
				{
					Items = new List<int>(other.Items.Count);
				}
				else
				{
					Items.Clear();
				}

				for (int i = 0; i < other.Items.Count; i++)
				{
					Items.Add(other.Items[i]);
				}
			}

			if (CitySpeed != null && CitySpeed.Count == other.CitySpeed.Count)
			{
				for (int i = 0; i < other.CitySpeed.Count; i++)
				{
					CitySpeed[i] = other.CitySpeed[i];
				}
			}
			else
			{
				if (CitySpeed == null)
				{
					CitySpeed = new List<float>(other.CitySpeed.Count);
				}
				else
				{
					CitySpeed.Clear();
				}

				for (int i = 0; i < other.CitySpeed.Count; i++)
				{
					CitySpeed.Add(other.CitySpeed[i]);
				}
			}

			//for (int i = 0; i < other.Cities.Count; i++)
			//{
			//	Cities[i] = other.Cities[i];
			//	Items[i] = other.Items[i];
			//	CitySpeed[i] = other.CitySpeed[i];
			//}

			_Score = other._Score;
			Configuration = other.Configuration;
		}

		private float CalculateDistance()
		{
			float score = 0;
			var distances = Configuration.Distances;
			for (int i = 0; i < Cities.Count - 1; i++)
			{
				score += distances[Cities[i], Cities[i + 1]];
			}

			score += distances[Cities.Last(), Cities[0]];
			return 200 - score;
		}

		public float CalculateScore()
		{
			//return CalculateDistance();
			return GFunction() - FFunction();
		}

		public float FFunction()
		{
			float score = 0;
			var distances = Configuration.Distances;
			for (int i = 0; i < Cities.Count-1; i++)
			{
				score += (distances[Cities[i], Cities[i + 1]] / CitySpeed[i]);
			}

			score += (distances[Cities.Last(), Cities[0]] / CitySpeed[Cities.Count-1]);
			return score;
		}

		public float GFunction()
		{
			int profit = 0;
			for (int i = 0; i < Items.Count; i++)
			{
				if (Items[i] < 0) continue;
				profit += Configuration.Items[Items[i]].Profit;
			}

			return profit;
		}

		public void Reproduce(Thief other, Thief child)
		{
			int index = 0;

			for (int i = 0; i < ReproduceArray.Length; i++) ReproduceArray[i] = -1;

			for (int i = 0; i < Cities.Count; i++)
			{
				if (Array.IndexOf(ReproduceArray, Cities[index]) < 0)
				{
					ReproduceArray[index] = Cities[index];
					index = Cities.IndexOf(other.Cities[index]);
				}
				else
				{
					for (int j = 0; j < ReproduceArray.Length; j++)
					{
						if (ReproduceArray[j] == -1)
						{
							ReproduceArray[j] = other.Cities[j];
						}
					}

					break;
				}
			}

			_Score = null;
		}

		public void TryMutate()
		{
			if (Random.NextDouble() < Configuration.MutationProbability)
			{
				Mutate();
			}
			Validate();
		}

		public void Validate()
		{
			///
			/// If mutation or reproduce can invalid cities use commented code
			/// 
			//HashSet<int> missingsHashSet = new HashSet<int>();
			//missingsHashSet.UnionWith( Enumerable.Range(0, Cities.Count) );
			//List<int> doubled = new List<int>();

			//for (int i = 0; i < Cities.Count; i++)
			//{
			//	if (missingsHashSet.Contains(Cities[i]))
			//	{
			//		missingsHashSet.Remove(Cities[i]);
			//	}
			//	else
			//	{
			//		doubled.Add(i);
			//	}
			//}

			//if (missingsHashSet.Count > 0)
			//{
			//	var missings = missingsHashSet.ToList();
			//	for (int i = 0; i < missings.Count; i++)
			//	{
			//		var index = doubled.Last();
			//		doubled.RemoveAt(doubled.Count-1);
			//		Cities[index] = missings[i];
			//	}
			//}

			if(_Score == null) FillItems();
		}

		private void Mutate()
		{
			int randomPosition1 = Random.Next(0, Cities.Count);
			int randomPosition2 = Random.Next(0, Cities.Count);
			int tmp = Cities[randomPosition1];
			Cities[randomPosition1] = Cities[randomPosition2];
			Cities[randomPosition2] = tmp;
			_Score = null;
		}

		private float GetCurrentSpeed(int cityIndex)
		{
			int wc = 0;

			var cities = Configuration.Items;

			for (int i = 0; i < cityIndex; i++)
			{
				if(Items[i] < 0) continue;
				wc += cities[Items[i]].Weight;
			}

			return Speed.Max - ( wc * ( (Speed.Max - Speed.Min) / KnapsackCapacity ) );
		}

		private void GetCurrentSpeed()
		{
			if (CitySpeed == null)
			{
				CitySpeed = new List<float>(Cities.Count);
			}
			else
			{
				CitySpeed.Clear();
			}

			int wc = 0;

			var cities = Configuration.Items;

			for (int i = 0; i < Cities.Count; i++)
			{
				if ((Items[i] < 0) == false)
				{
					wc += cities[Items[i]].Weight;
				}

				CitySpeed.Add(Speed.Max - (wc * ((Speed.Max - Speed.Min) / KnapsackCapacity)));
			}
		}

		private void FillItems()
		{
			if (Items == null)
			{
				Items = new List<int>();
			}
			else
			{
				Items.Clear();
			}

			int weight = 0;
			foreach (var city in Cities)
			{
				bool choose = false;
				var items = Configuration.ItemsInCities[city];
				for (int i = 0; i < items.Length; i++)
				{
					if (KnapsackCapacity >= weight + Configuration.Items[Configuration.ItemsInCities[city][i]].Weight)
					{
						Items.Add(Configuration.ItemsInCities[city][i]);
						choose = true;
						weight += Configuration.Items[Configuration.ItemsInCities[city][i]].Weight;
						break;
					}
				}
				if(choose == false) Items.Add(-1);
			}

			GetCurrentSpeed();
		}

		private static List<E> ShuffleList<E>(List<E> inputList)
		{
			List<E> randomList = new List<E>();

			Random r = new Random();
			int randomIndex = 0;
			while (inputList.Count > 0)
			{
				randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
				randomList.Add(inputList[randomIndex]); //add it to the new, random list
				inputList.RemoveAt(randomIndex); //remove to avoid duplicates
			}

			return randomList; //return the new random list
		}
	}
}
