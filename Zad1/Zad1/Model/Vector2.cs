﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1.Model
{
	public struct Vector2
	{
		public float X ;
		public float Y;

		public Vector2(float x, float y)
		{
			X = x;
			Y = y;
		}

		public static float Distance(Vector2 a, Vector2 b)
		{
			return (float) Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
		}

		public override string ToString()
		{
			return $"({X}, {Y})";
		}
	}
}
