﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Zad1.Model
{
	public class Population
	{
		Random Random = new Random();

		public List<Thief> Thieves;
		public List<Thief> NextGeneration;
		public List<int> SelectedToNextGeneration;
		public int PopulationSize;
		public float CrossProbability;
		public float MutationProbability => Configuration.MutationProbability;
		public int MaxGenerations;
		public float MaxScore;
		public int Tour;
		public readonly Configuration Configuration;

		public int CurrentGeneration { get; private set; }

		public string Log => Logger.ToString();
		private readonly StringBuilder Logger = new StringBuilder();

		private readonly Stopwatch Stopwatch = new Stopwatch();

		#region DONE_FUNCTIONS

		public bool IsMaxGeneration()
		{
			return CurrentGeneration >= MaxGenerations;
		}

		public bool IsGoodScore()
		{
			return Thieves[0].Score >= MaxScore;
		}

		#endregion
		public Func<bool> IsDone; 

		public Population(int populationSize, float crossProbability, int maxGenerations, float maxScore, int tour, Configuration configuration)
		{
			PopulationSize = populationSize;
			CrossProbability = crossProbability;
			MaxGenerations = maxGenerations;
			MaxScore = maxScore;
			Tour = tour;
			Configuration = configuration;

			Thieves = new List<Thief>(populationSize);
			NextGeneration = new List<Thief>(populationSize);
			SelectedToNextGeneration = new List<int>(populationSize);

			int[] reproduceArray = new int[configuration.Cities.Count];

			for (int i = 0; i < populationSize; i++)
			{
				NextGeneration.Add(Thief.GetRandom(Configuration, reproduceArray));
				Thieves.Add(new Thief(reproduceArray));
				SelectedToNextGeneration.Add(0);
			}

		}

		public void Simulate()
		{
			CurrentGeneration = 0;
			Evaluate();

			while (IsDone() == false)
			{
				Stopwatch.Start();
				if (CurrentGeneration % 100 == 0)
				{
					Console.WriteLine($"Process {CurrentGeneration} generation");
				}

				Selection();
				Reproduce();
				Mutate();
				CurrentGeneration++;
				Evaluate();
				Stopwatch.Reset();
			}
		}

		private void Evaluate()
		{
			for (int i = 0; i < Thieves.Count; i++)
			{
				Thieves[i].Copy(NextGeneration[i]);
			}
			
			Thieves.Sort((a, b) => b.Score.CompareTo(a.Score));
			CreateLog();
		}

		private void Selection()
		{
			if (Configuration.CrossingType == Configuration.CrossType.Random)
			{
				for (int i = 0; i < Thieves.Count; i++)
				{
					SelectedToNextGeneration[i] = (Random.Next(0, NextGeneration.Count));
				}
			}
			else
			{
				for (int i = 0; i < Thieves.Count; i++)
				{
					SelectedToNextGeneration[i] = (Tournament());
				}
			}
		}

		private void Reproduce()
		{
			for (int i = 0; i < NextGeneration.Count; i++)
			{
				if (Random.NextDouble() < CrossProbability)
				{
					Thieves[SelectedToNextGeneration[i]].Reproduce(
						Thieves[Configuration.CrossingType == Configuration.CrossType.Tour ? Tournament() : Random.Next(0, NextGeneration.Count)],
						NextGeneration[i]);
				}
				else
				{
					NextGeneration[i].Copy(Thieves[SelectedToNextGeneration[i]]);
				}
			}
		}

		private void Mutate()
		{
			for (int i = 0; i < NextGeneration.Count; i++)
			{
				NextGeneration[i].TryMutate();
			}
		}

		private void CreateLog()
		{
			var sum = Thieves.Sum(t => t.Score);
			Logger.Append(CurrentGeneration);
			Logger.Append(';');
			Logger.Append(Thieves[0].Score);
			Logger.Append(';');
			Logger.Append(sum / Thieves.Count);
			Logger.Append(';');
			Logger.Append(Thieves.Last().Score);
			Logger.Append(';');
			Logger.Append(Stopwatch.Elapsed.TotalMilliseconds);

			Logger.Append(';');
			Logger.Append(Configuration.DataName);
			Logger.Append(';');
			Logger.Append(Thieves.Count);
			Logger.Append(';');
			Logger.Append(CrossProbability);
			Logger.Append(';');
			Logger.Append(MaxGenerations);
			Logger.Append(';');
			Logger.Append(Tour);
			Logger.Append(';');
			Logger.Append(MutationProbability);

			Logger.Append('\n');
		}

		private int Tournament()
		{
			int winner = PopulationSize;
			for (int i = 0; i < Tour; i++)
			{
				winner = Math.Min(winner, Random.Next(0, PopulationSize));
			}

			return winner;
		}
	}
}
