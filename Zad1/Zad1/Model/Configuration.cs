﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using Zad1.Utils;
using System.Linq;

namespace Zad1.Model
{
	public class Configuration
	{
		public string DataName { get; set; }
		public string Name { get; private set; }
		public int KnapsackCapacity { get; private set; }
		public MinMax<float> Speed { get; private set; }
		public float RentingRatio { get; private set; }

		public readonly List<City> Cities;
		public readonly List<Item> Items;

		public readonly float[,] Distances;

		public readonly int[][] ItemsInCities;

		public float MutationProbability = 0.1f;

		public enum CrossType { Random, TourRandom, Tour}

		public CrossType CrossingType;

		public Configuration(string name, int knapsackCapacity, MinMax<float> speed, float rentingRatio, List<City> cities, List<Item> items)
		{
			Name = name;
			KnapsackCapacity = knapsackCapacity;
			Speed = speed;
			RentingRatio = rentingRatio;
			Cities = cities;
			Items = items;

			Distances = new float[Cities.Count, Cities.Count];

			for(int i = 0; i < Cities.Count; i++)
			{
				for (int j = 0; j < Cities.Count; j++)
				{
					Distances[i, j] = Vector2.Distance(Cities[i].Position, Cities[j].Position);
				}
			}

			for (int i = 0; i < Items.Count; i++)
			{
				Items[i].ProfitRatio = Items[i].Profit / (float) Items[i].Weight;
			}

			ItemsInCities = new int[Cities.Count][];
			for (int i = 0; i < Cities.Count; i++)
			{
				City city = Cities[i];
				int cityIndex = city.Index;
				var itemsInCity = Items.Where(item => item.CityIndex == cityIndex).ToList();
				itemsInCity.Sort((a, b) => a.ProfitRatio.CompareTo(b.ProfitRatio));
				ItemsInCities[i] = new int[itemsInCity.Count];
				for (int j = 0; j < ItemsInCities[i].Length; j++)
				{
					ItemsInCities[i][j] = Items.IndexOf(itemsInCity[j]);
				}
			}
		}

		public static Configuration Load(string filePath)
		{
			var lines = DataFileLoader.Load(filePath);
			if (lines == null) return null;

			string name = lines[DataFileLoader.NAME_KEY];
			int knapscakCapacity = int.Parse(lines[DataFileLoader.KNAPSACK_CAPACITY_KEY]);
			MinMax<float> speed = new MinMax<float>(float.Parse(lines[DataFileLoader.SPEED_MIN_KEY]),
													float.Parse(lines[DataFileLoader.SPEED_MAX_KEY]));
			float rentingRatio = float.Parse(lines[DataFileLoader.RENTING_KEY]);

			List<City> cities = DataFileLoader.LoadCitiesFromFile(lines);
			List<Item> items = DataFileLoader.LoadItemsFromFile(lines);

			return new Configuration(name, knapscakCapacity, speed, rentingRatio, cities, items)
			{
				DataName = filePath.Split('\\', '/')[1].Split('.')[0],
			};
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("Name: ");
			sb.Append(Name);
			sb.Append(", Knapsack capacity: ");
			sb.Append(KnapsackCapacity);
			sb.Append(", Speed: ");
			sb.Append(Speed);
			sb.Append(", Renting ratio: ");
			sb.Append(RentingRatio);
			sb.Append(", Cities count: ");
			sb.Append(Cities.Count);
			sb.Append(", Items count: ");
			sb.Append(Items.Count);
			return sb.ToString();
		}
	}
}
