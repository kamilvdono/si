﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1.Model
{
	public class City
	{
		public int Index;
		public Vector2 Position;

		public override string ToString()
		{
			return $"City: {Index}, {Position}";
		}
	}
}
