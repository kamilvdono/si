﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1.Utils
{
	public struct MinMax<T> where T : IComparable
	{
		private T _Min;

		public T Min
		{
			get => _Min;
			set
			{
				_Min = value;
				Validate();
			}
		}

		private T _Max;

		public T Max
		{
			get => _Max;
			set
			{
				_Max = value;
				Validate();
			}
		}

		public MinMax(T min, T max)
		{
			_Min = min;
			_Max = max;
			Validate();
		}

		private void Validate()
		{
			if (Min.CompareTo(Max) > 0)
			{
				var tmp = Min;
				_Min = _Max;
				_Max = tmp;
			}
		}

		public override string ToString()
		{
			return $"[{Min}, {Max}]";
		}
	}
}
