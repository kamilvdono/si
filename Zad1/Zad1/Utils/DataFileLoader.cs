﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Zad1.Model;

namespace Zad1.Utils
{
	public static class DataFileLoader
	{
		public const char LINE_SEPARATOR = ':';
		public const string NAME_KEY = "PROBLEM NAME";
		public const string DIMENSION_KEY = "DIMENSION";
		public const string ITEMS_COUNT_KEY = "NUMBER OF ITEMS";
		public const string KNAPSACK_CAPACITY_KEY = "CAPACITY OF KNAPSACK";
		public const string SPEED_MIN_KEY = "MIN SPEED";
		public const string SPEED_MAX_KEY = "MAX SPEED";
		public const string RENTING_KEY = "RENTING RATIO";
		public const string CITIES_KEY = "NODE_COORD_SECTION	(INDEX, X, Y)";
		public const string ITEMS_KEY = "ITEMS SECTION\t(INDEX, PROFIT, WEIGHT, ASSIGNED NODE NUMBER)";

		public static Dictionary<string, string> Load(string filePath)
		{
			string file;
			try
			{
				file = File.ReadAllText(filePath);
			}
			catch (Exception e)
			{
				System.Console.WriteLine($"File read exception: {e.ToString()}");
				return null;
			}

			string[] lines = file.Split(new[] { System.Environment.NewLine }, StringSplitOptions.None);

			var processedLines = ProcessLines(lines);
			return processedLines;
		}

		public static List<City> LoadCitiesFromFile(Dictionary<string, string> parsedLines)
		{
			string[] citiesString = parsedLines[CITIES_KEY].Trim().Split(new[] { System.Environment.NewLine }, StringSplitOptions.None);
			List<City> cities = new List<City>(citiesString.Length);
			foreach (var cityString in citiesString)
			{
				string[] cityData = cityString.Split('\t');
				City city = new City()
				{
					Index = int.Parse(cityData[0]),
					Position = new Vector2(float.Parse(cityData[1]), float.Parse(cityData[2])),
				};
				cities.Add(city);
			}

			return cities;
		}

		public static List<Item> LoadItemsFromFile(Dictionary<string, string> parsedLines)
		{
			string[] itemsStrings = parsedLines[ITEMS_KEY].Trim().Split(new[] { System.Environment.NewLine }, StringSplitOptions.None);
			List<Item> items = new List<Item>(itemsStrings.Length);
			foreach (var itemString in itemsStrings)
			{
				string[] itemData = itemString.Split('\t');
				Item item = new Item()
				{
					Index = int.Parse(itemData[0]),
					Profit = int.Parse(itemData[1]),
					Weight = int.Parse(itemData[2]),
					CityIndex = int.Parse(itemData[3])
				};
				items.Add(item);
			}

			return items;
		}

		private static Dictionary<string, string> ProcessLines(string[] lines)
		{
			Dictionary<string, string> dataDictionary = new Dictionary<string, string>();
			for (int i = 0; i < lines.Length; i++)
			{
				if (lines[i].Contains(LINE_SEPARATOR) == false) continue;
				string[] linesSplitted = lines[i].Trim().Split(LINE_SEPARATOR);
				if (linesSplitted.Length == 2)
				{
					if (string.IsNullOrEmpty(linesSplitted[1]) == false)
					{
						dataDictionary.Add(linesSplitted[0].Trim(), linesSplitted[1].Trim());
					}
					else
					{
						StringBuilder sb = new StringBuilder();
						while (++i < lines.Length && (lines[i].Contains(LINE_SEPARATOR) == false))
						{
							sb.AppendLine(lines[i].Trim());
						}

						dataDictionary.Add(linesSplitted[0].Trim(), sb.ToString());

						i--;
					}
				}
			}

			return dataDictionary;
		}

		public static void ProcessLogs()
		{
			var allFiles = System.IO.Directory.GetFiles("Out").Select(d => d.Split('\\')[1]).ToList();
			Dictionary<string, List<string>> pathClasses = new Dictionary<string, List<string>>();

			allFiles.ForEach(fl =>
			{
				string classs = fl.Split("_ID")[0];
				if (pathClasses.TryGetValue(classs, out var pathes) == false)
				{
					pathes = new List<string>();
					pathClasses.Add(classs, pathes);
				}
				pathes.Add(fl);
			});

			foreach (var pathClassesKey in pathClasses.Keys)
			{
				var paths = pathClasses[pathClassesKey];
				if(paths.Count < 2) continue;
				List<LogData> logs = new List<LogData>(paths.Count);
				foreach (var path in paths)
				{
					var lines = File.ReadAllLines($"Out/{path}");
					var logData = new LogData();
					logs.Add(logData);
					logData.Parse(lines);
					File.Delete($"Out/{path}");
				}

				var log = LogData.Combine(logs);
				File.WriteAllText($"Out/{pathClassesKey}.csv", log.ToString().Replace('.',','));
			}
		}
	}

	public class LogData
	{
		List<GenerationLogData> data = new List<GenerationLogData>();
		private string dataName;
		private int pop;
		private double cross;
		private int maxGen;
		private int tour;
		private double mut;

		public void Parse(string[] text)
		{
			foreach (string lineWrong in text)
			{
				var line = lineWrong.Replace(',', '.');
				var sep = line.Split(';');
				if(sep.Length != 11) continue;
				if (!int.TryParse(sep[0], out var nr)) continue;
				var gen = new GenerationLogData()
				{
					Best = double.Parse(sep[1]),
					Avg = double.Parse(sep[2]),
					Worse = double.Parse(sep[3]),
					Time = double.Parse(sep[4]),
				};

				if (string.IsNullOrEmpty(dataName))
				{
					dataName = sep[5];
					pop = int.Parse(sep[6]);
					cross = double.Parse(sep[7]);
					maxGen = int.Parse(sep[8]);
					tour = int.Parse(sep[9]);
					mut = double.Parse(sep[10]);
				}

				data.Add(gen);
			}
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder("Nr pokolenia;Najlepsza ocena;Srednia ocena;Najgorsza ocena;Czas generacji;Odchylenie;Plik;Rozmiar populacji;Prawdopodobienstwo rozmnozenia;Maksymalna ilosc generacji;Rozmiar turnieju;Prawdopodobienstwo mutacji\n");
			for (int i = 0; i < data.Count; i++)
			{
				sb.AppendLine($"{i};{data[i].ToString()};{dataName};{pop};{cross};{maxGen};{tour};{mut}");
			}

			return sb.ToString();
		}

		public static LogData Combine(List<LogData> datas)
		{
			int count = datas[0].data.Count;
			LogData r = new LogData()
			{
				dataName = datas[0].dataName,
				pop = datas[0].pop,
				cross = datas[0].cross,
				maxGen = datas[0].maxGen,
				tour = datas[0].tour,
				mut = datas[0].mut,
			};

			for (int i = 0; i < count; i++)
			{
				var newGen = GenerationLogData.Copy(datas[0].data[i]);
				r.data.Add(newGen);
				for (int j = 1; j < datas.Count; j++)
				{
					newGen.MakeAvg(datas[j].data[i]);
				}
			}

			for (int i = 0; i < count; i++)
			{
				var avg = r.data[i].Best;
				double xi = 0;
				for (int j = 0; j < datas.Count; j++)
				{
					var tm = (datas[j].data[i].Best - avg);
					xi += tm * tm;
				}

				xi = xi / count;

				r.data[i].Deviance = Math.Sqrt(xi);
			}

			return r;
		}
	}

	public class GenerationLogData
	{
		public double Best;
		public double Avg;
		public double Worse;
		public double Time;
		public double Deviance;

		public void MakeAvg(GenerationLogData other)
		{
			Best = (Best + other.Best) / 2.0;
			Avg = (Avg + other.Avg) / 2.0;
			Worse = (Worse + other.Worse) / 2.0;
			Time = (Time + other.Time) / 2.0;
		}

		public static GenerationLogData Copy(GenerationLogData other)
		{
			GenerationLogData r = new GenerationLogData();
			r.Best = other.Best;
			r.Avg = other.Avg;
			r.Worse = other.Worse;
			r.Time = other.Time;
			return r;
		}

		public override string ToString()
		{
			return $"{Best};{Avg};{Worse};{Time};{Deviance}";
		}
	}
}
