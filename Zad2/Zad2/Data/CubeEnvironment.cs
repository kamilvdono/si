﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad2.Data
{
	abstract class CubeEnvironment : IHTMLView
	{
		public string Name;
		public int[,] Board;
		public List<int>[,] Domain;
		public List<ForwardChange>[,] DomainChanges;
		public int Size;
		public List<BoardField> Heuristic;
		public int[,] DomainIndex;

		public virtual void InitBoard(int n)
		{
			Board = new int[n, n];
			Size = n;
			Domain = new List<int>[n, n];
			DomainIndex = new int[n,n];
			for (int x = 0; x < n; x++)
			{
				for (int y = 0; y < n; y++)
				{
					Domain[x, y] = new List<int>();
					DomainIndex[x, y] = -1;
					for (int i = 1; i <= Size; i++)
					{
						Domain[x, y].Add(i);
					}
				}
			}
			DomainChanges = new List<ForwardChange>[n,n];
			for (int x = 0; x < n; x++)
			{
				for (int y = 0; y < n; y++)
				{
					DomainChanges[x, y] = new List<ForwardChange>(Size * 3);
				}
			}
		}

		public virtual bool AreConstrainsValid()
		{
			for (int x = 0; x < Board.GetLength(0); x++)
			{
				for (int y = 0; y < Board.GetLength(1); y++)
				{
					if (AreConstrainsValid(x, y) == false) return false;
				}
			}

			return true;
		}

		public virtual bool AreConstrainsValid(int x, int y)
		{
			int value = Board[x, y];
			if (value == 0)
			{
				if (Domain[x, y].Count == 0)
				{
					return false;
				}
				return true;
			}
			for (int x2 = 0; x2 < Size; x2++)
			{
				if (Board[x2, y] == value && (x2 != x))
				{
					return false;
				}
			}

			for (int y2 = 0; y2 < Size; y2++)
			{
				if (Board[x, y2] == value && (y2 != y))
				{
					return false;
				}
			}

			return true;
		}

		public abstract CubeEnvironment Clone();
		public abstract string GetInHTML();

		public void FillBoard(BoardField field, int value)
		{
			Board[field.X, field.Y] = value;
		}

		public virtual void ForwardEliminate()
		{
			for (int x = 0; x < Size; x++)
			{
				for (int y = 0; y < Size; y++)
				{
					if (Board[x, y] != 0)
					{
						FilledField(new BoardField(x, y), Board[x,y]);
					}
				}
			}
		}

		public virtual bool ForwardCheck(int x, int y)
		{
			BoardField field = new BoardField(x,y);
			var value = Board[x, y];
			var changes = DomainChanges[field.X, field.Y];
			foreach (var d in Domain[field.X, field.Y])
			{
				changes.Add(new ForwardChange() { Field = field, Value = d });
			}
			Domain[field.X, field.Y].Clear();
			for (int i = 0; i < Size; i++)
			{
				if (Domain[i, field.Y].Contains(value))
				{
					Domain[i, field.Y].Remove(value);
					changes.Add(new ForwardChange() { Field = new BoardField(i, field.Y), Value = value });
				}

				if (Domain[field.X, i].Contains(value))
				{
					Domain[field.X, i].Remove(value);
					changes.Add(new ForwardChange() { Field = new BoardField(field.X, i), Value = value });
				}
			}

			for (int i = 0; i < Size; i++)
			{
				if (Board[i, y] == 0 && Domain[i, y].Count == 0)
				{
					return false;
				}

				if (Board[x, i] == 0 && Domain[x, i].Count == 0)
				{
					return false;
				}
			}

			return true;
		}

		protected void FilledField(BoardField field, int value)
		{
			var changes = DomainChanges[field.X, field.Y];
			foreach (var d in Domain[field.X, field.Y])
			{
				changes.Add(new ForwardChange() { Field = field, Value = d });
			}
			Domain[field.X, field.Y].Clear();
			for (int i = 0; i < Size; i++)
			{
				if (Domain[i, field.Y].Contains(value))
				{
					Domain[i, field.Y].Remove(value);
					changes.Add(new ForwardChange() { Field = new BoardField(i, field.Y), Value = value });
				}

				if (Domain[field.X, i].Contains(value))
				{
					Domain[field.X, i].Remove(value);
					changes.Add(new ForwardChange() { Field = new BoardField(field.X, i), Value = value });
				}
			}
		}

		public List<int>[,] CloneDomain()
		{
			List<int>[,] newDomain = new List<int>[Size, Size];

			for (int i = 0; i < Size; i++)
			{
				for (int j = 0; j < Size; j++)
				{
					newDomain[i, j] = new List<int>(Domain[i, j].Count);
					foreach (var i1 in Domain[i,j])
					{
						newDomain[i, j].Add(i1);
					}
				}
			}

			return newDomain;
		}

		public List<ForwardChange>[,] CloneDomainChanges()
		{
			List<ForwardChange>[,] newDomain = new List<ForwardChange>[Size, Size];

			for (int i = 0; i < Size; i++)
			{
				for (int j = 0; j < Size; j++)
				{
					newDomain[i, j] = new List<ForwardChange>(DomainChanges[i, j].Capacity);
					for (int k = 0, no = DomainChanges[i, j].Count; k < no; k++)
					{
						newDomain[i, j].Add(DomainChanges[i, j][k]);
					}
				}
			}

			return newDomain;
		}

		public void CalcHeuristic()
		{
			Heuristic = new List<BoardField>(Size * Size);
			for (int x = 0; x < Size; x++)
			{
				for (int y = 0; y < Size; y++)
				{
					if (Board[x, y] == 0)
					{
						Heuristic.Add(new BoardField(x,y));
					}
				}
			}
		}

		public struct ForwardChange
		{
			public BoardField Field;
			public int Value;
		}
	}
}
