﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Zad2.Data
{
	class CubeSolver
	{

		public static Tuple<CubeEnvironment, bool> Solve(CubeEnvironment environment, bool debug = false)
		{
			if (environment.AreConstrainsValid() == false)
			{
				return new Tuple<CubeEnvironment, bool>(environment, false);
			}

			environment = environment.Clone();

			var heuristic = environment.Heuristic;
			for (int i = 0, no = heuristic.Count; i < no; i++)
			{
				if (i < 0)
				{
					return new Tuple<CubeEnvironment, bool>(environment, false);
				}
				int x = heuristic[i].X;
				int y = heuristic[i].Y;
				var domainIndex = ++environment.DomainIndex[x, y];
				var domain = environment.Domain[x, y];
				if (domainIndex >= domain.Count)
				{
					i -= 2;
					environment.DomainIndex[x, y] = -1;
					environment.Board[x, y] = 0;
					continue;
				}

				environment.Board[x, y] = domain[domainIndex];
				if (debug) File.WriteAllText("Dane/Out/" + Path.GetFileNameWithoutExtension(environment.Name) + DateTime.Now.Ticks.ToString() + ".html", environment.GetInHTML());
				if (environment.AreConstrainsValid(x, y) == false)
				{
					environment.Board[x, y] = 0;
					i -= 1;
				}
			}

			return new Tuple<CubeEnvironment, bool>(environment, true);
		}

		private static Tuple<CubeEnvironment, bool> Solve(int x, int y, CubeEnvironment environment, bool debug)
		{
			var size = environment.Size;
			
			x += (y / size);
			y = y % size;

			while (x < size)
			{
				if (environment.Board[x, y] == 0)
				{
					var domain = environment.Domain[x, y];
					foreach (var dom in domain)
					{
						environment.Board[x, y] = dom;
						if (environment.AreConstrainsValid(x, y))
						{
							if (debug) File.WriteAllText("Dane/Out/" + Path.GetFileNameWithoutExtension(environment.Name) + DateTime.Now.Ticks.ToString() + ".html", environment.GetInHTML());
							var higher = Solve(x, y + 1, environment, debug);
							if (higher.Item2)
							{
								return higher;
							}
						}
					}
					environment.Board[x, y] = 0;
					return new Tuple<CubeEnvironment, bool>(environment, false);
				}

				y++;
				x += (y / size);
				y = y % size;
			}

			return new Tuple<CubeEnvironment, bool>(environment, true);
		}

		public static Tuple<CubeEnvironment, bool> SolveForward(CubeEnvironment environment, bool debug = false)
		{
			if (environment.AreConstrainsValid() == false)
			{
				return new Tuple<CubeEnvironment, bool>(environment, false);
			}

			environment = environment.Clone();

			var heuristic = environment.Heuristic;
			for (int i = 0, no = heuristic.Count; i < no; i++)
			{
				if (i < 0)
				{
					return new Tuple<CubeEnvironment, bool>(environment, false);
				}
				int x = heuristic[i].X;
				int y = heuristic[i].Y;
				var domainIndex = ++environment.DomainIndex[x, y];
				var domain = environment.Domain[x, y];
				if (domainIndex >= domain.Count)
				{
					//Clear myself
					environment.DomainIndex[x, y] = -1;
					environment.Board[x, y] = 0;
					var changes = environment.DomainChanges[x, y];
					for (int j = 0, nj = changes.Count; j < nj; j++)
					{
						var field = changes[j];
						environment.Domain[field.Field.X, field.Field.Y].Add(field.Value);
					}
					changes.Clear();
					//Clear parent
					i--;
					if (i < 0)
					{
						return new Tuple<CubeEnvironment, bool>(environment, false);
					}
					x = heuristic[i].X;
					y = heuristic[i].Y;
					environment.Board[x, y] = 0;
					changes = environment.DomainChanges[x, y];
					for (int j = 0, nj = changes.Count; j < nj; j++)
					{
						var field = changes[j];
						environment.Domain[field.Field.X, field.Field.Y].Add(field.Value);
					}
					changes.Clear();

					i--;
					continue;
				}

				environment.Board[x, y] = domain[domainIndex];
				if (debug) File.WriteAllText("Dane/Out/" + Path.GetFileNameWithoutExtension(environment.Name) + DateTime.Now.Ticks.ToString() + ".html", environment.GetInHTML());
				var constr = environment.AreConstrainsValid(x, y);
				var forward = environment.ForwardCheck(x, y);
				if (constr == false || forward == false)
				{
					environment.Board[x, y] = 0;
					i -= 1;
					var changes = environment.DomainChanges[x, y];
					for (int j = 0, nj = changes.Count; j < nj; j++)
					{
						var field = changes[j];
						environment.Domain[field.Field.X, field.Field.Y].Add(field.Value);
					}
					changes.Clear();
				}
			}

			return new Tuple<CubeEnvironment, bool>(environment, true);
		}

		public static Tuple<CubeEnvironment, bool> SolveForward(int x, int y, CubeEnvironment environment, bool debug)
		{
			var size = environment.Size;
			x += (y / size);
			y = y % size;

			while (x < size)
			{
				if (environment.Board[x, y] == 0)
				{
					var domainCopy = new List<int>(environment.Domain[x, y]);
					for (int i = 0, no = domainCopy.Count; i < no; i++)
					{
						environment.Board[x, y] = domainCopy[i];
						if (environment.AreConstrainsValid(x, y) && environment.ForwardCheck(x, y))
						{
							if(debug) File.WriteAllText("Dane/Out/" + Path.GetFileNameWithoutExtension(environment.Name) + DateTime.Now.Ticks.ToString() + ".html", environment.GetInHTML());
							var higher = SolveForward(x, y + 1, environment, debug);
							if (higher.Item2)
							{
								return higher;
							}
						}

						var changes = environment.DomainChanges[x, y];
						if (changes != null)
						{
							for (int j = 0, nj = changes.Count; j < nj; j++)
							{
								var field = changes[j];
								environment.Domain[field.Field.X, field.Field.Y].Add(field.Value);
							}
						}
						changes.Clear();
					}
					environment.Board[x, y] = 0;
					return new Tuple<CubeEnvironment, bool>(environment, false);
				}

				y++;
				x += (y / size);
				y = y % size;
			}

			return new Tuple<CubeEnvironment, bool>(environment, true);
		}
	}
}
