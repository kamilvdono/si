﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad2.Data
{
	static class DataLoader
	{

		public static CubeEnvironment Load(string filePath)
		{
			if (filePath.Contains("futo"))
			{
				return FutoshikiDataLoader.LoadFutoshikiEnvironment(filePath);
			}
			if (filePath.Contains("sky"))
			{
				return SkyscrapperDataLoader.LoadSkyscrapperEnvironment(filePath);
			}
			
			Console.WriteLine("Do not recognize file");
			return null;
		}

	}
}
