﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad2.Data
{
	public class Relation : IComparable
	{
		public BoardField Smaller;
		public BoardField Bigger;

		public BoardField Min;
		public BoardField Max;

		public Relation(BoardField smaller, BoardField bigger)
		{
			Smaller = smaller;
			Bigger = bigger;

			Min = new BoardField(Math.Min(Smaller.X, Bigger.X), Math.Min(Smaller.Y, Bigger.Y));
			Max = new BoardField(Math.Max(Smaller.X, Bigger.X), Math.Max(Smaller.Y, Bigger.Y));
		}

		public int CompareTo(object obj)
		{
			Relation other = obj as Relation;
			if (other == null) return 1;

			if (Min.X - other.Min.X >= 1) return 1;
			if (other.Min.X - Min.X >= 1) return -1;

			if (Smaller.Y == Bigger.Y)
			{
				if (other.Smaller.Y == other.Bigger.Y)
				{
					return Min.Y.CompareTo(other.Min.Y);
				}
				else
				{
					return 1;
				}
			}
			else
			{
				if (other.Smaller.Y == other.Bigger.Y)
				{
					return -1;
				}
				else
				{
					return Min.Y.CompareTo(other.Min.Y);
				}
			}
		}

		public bool HTMLField(int x, int y, bool vertical = false)
		{
			if (Min.Y != y) return false;

			if (vertical)
			{
				if (Bigger.X == Smaller.X)
				{
					return false;
				}
				return Min.X == x;
			}

			if (Bigger.X != Smaller.X)
			{
				return false;
			}
			return Min.X == x;
		}

		public string GetSign()
		{
			if (Bigger.X == Smaller.X)
			{
				if (Smaller.Y < Bigger.Y)
				{
					return "<";
				}

				return ">";
			}
			else
			{
				if (Smaller.X < Bigger.X)
				{
					return "^";
				}

				return "\u2228";
			}
		}
	}
}
