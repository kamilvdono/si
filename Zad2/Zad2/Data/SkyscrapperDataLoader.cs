﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Zad2.Data
{
	static class SkyscrapperDataLoader
	{
		public static SkyscrapperEnvironment LoadSkyscrapperEnvironment(string filePath)
		{
			if (File.Exists(filePath) == false)
			{
				Console.WriteLine($"There is no file at path {filePath}");
				return null;
			}

			var fileLines= File.ReadAllLines(filePath);
			var n = int.Parse(fileLines[0]);

			SkyscrapperEnvironment environment = new SkyscrapperEnvironment();
			environment.InitBoard(n);

			foreach (var side in SideEnum.AllEnums)
			{
				var sideLine = fileLines.First(line => line.Contains(side.CharacterSign));
				var values = sideLine.Split(';');
				for (int i = 1; i < values.Length; i++)
				{
					environment.SetEdge(side, i-1, int.Parse(values[i]));
				}
			}

			environment.Name = Path.GetFileName(filePath);

			return environment;
		}
	}
}
