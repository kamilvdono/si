﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zad2.Data
{
	class FutoshikiEnvironment : CubeEnvironment
	{
		public List<Relation> Relations = new List<Relation>();

		public void AddRelation(char x0, int y0, char x1, int y1)
		{
			Relation relation = new Relation(
				new BoardField() {X = ((int) x0 - 65), Y = y0},
				new BoardField() {X = ((int) x1 - 65), Y = y1}
			);

			Relations.Add(relation);
		}

		public override void ForwardEliminate()
		{
			base.ForwardEliminate();

			for (int i = 0, no = Relations.Count; i < no; i++)
			{
				ForwardEliminateRelation(Relations[i]);
			}
		}

		private void ForwardEliminateRelation(Relation relation)
		{
			var sDomain = Domain[relation.Smaller.X, relation.Smaller.Y];
			if (sDomain.Contains(Size))
			{
				sDomain.Remove(Size);
			}
			var bDomain = Domain[relation.Bigger.X, relation.Bigger.Y];
			if (bDomain.Contains(1))
			{
				bDomain.Remove(1);
			}

			int min = Board[relation.Smaller.X, relation.Smaller.Y] != 0 ? Board[relation.Smaller.X, relation.Smaller.Y] : Domain[relation.Smaller.X, relation.Smaller.Y].DefaultIfEmpty(0).Min();
			for (int i = min; i >= 1; i--)
			{
				if (bDomain.Contains(i))
				{
					bDomain.Remove(i);
				}
			}

			
			int max = Board[relation.Bigger.X, relation.Bigger.Y] != 0 ? Board[relation.Bigger.X, relation.Bigger.Y] : Domain[relation.Bigger.X, relation.Bigger.Y].DefaultIfEmpty(Size+1).Max();
			for (int i = max; i <= Size; i++)
			{
				if (sDomain.Contains(i))
				{
					sDomain.Remove(i);
				}
			}
		}

		public override CubeEnvironment Clone()
		{
			FutoshikiEnvironment clone = new FutoshikiEnvironment();
			clone.InitBoard(Size);
			clone.Relations = Relations;
			clone.Name = Name;

			for (int x = 0; x < Size; x++)
			{
				for (int y = 0; y < Size; y++)
				{
					clone.Board[x, y] = Board[x, y];
					
				}
			}
			clone.Heuristic = new List<BoardField>(Heuristic);
			clone.Domain = CloneDomain();
			clone.DomainChanges = CloneDomainChanges();

			return clone;
		}

		public override string GetInHTML()
		{
			int lastRelation = 0;

			StringBuilder sb = new StringBuilder();
			sb.Append($"<html>\r\n<body><h3>{Name}</h3>\r\n\t<table>");

			for (int x = 0; x < Size; x++)
			{
				sb.AppendLine("<tr>");

				for (int y = 0; y < Size; y++)
				{
					sb.AppendLine("<td>");
					sb.AppendLine(Board[x,y].ToString());
					sb.AppendLine("</td>");
					sb.AppendLine("<td style=\"color: red; \">");
					if (lastRelation < Relations.Count && Relations[lastRelation].HTMLField(x, y))
					{
						sb.AppendLine(Relations[lastRelation].GetSign());
						lastRelation++;
					}
					sb.AppendLine("</td>");
				}

				sb.AppendLine("</tr>");

				sb.AppendLine("<tr>");

				for (int y = 0; y < Size; y++)
				{
					sb.AppendLine("<td style=\"color: red; \">");
					if (lastRelation < Relations.Count && Relations[lastRelation].HTMLField(x, y, true))
					{
						sb.AppendLine(Relations[lastRelation].GetSign());
						lastRelation++;
					}
					sb.AppendLine("</td>");
					sb.AppendLine("<td>");
					sb.AppendLine("</td>");
				}

				sb.AppendLine("</tr>");
			}

			sb.Append("</table>\r\n</body>\r\n</html>");
			return sb.ToString();
		}

		public void Valid()
		{
			Relations.Sort();
		}

		public override bool AreConstrainsValid(int x, int y)
		{
			if (Board[x, y] == 0)
			{
				return base.AreConstrainsValid(x, y);
			}
			for (int i = 0, no = Relations.Count; i < no; i++)
			{
				var relation = Relations[i];

				if (relation.Max.X - x > 1)
				{
					break;
				}

				if (relation.Smaller.Equals(x, y))
				{
					var bigger = Board[relation.Bigger.X, relation.Bigger.Y];
					if (Board[x, y] >= bigger && bigger != 0)
					{
						return false;
					}
				}
				if(relation.Bigger.Equals(x, y))
				{
					var smaller = Board[relation.Smaller.X, relation.Smaller.Y];
					if (Board[x, y] <= smaller && smaller != 0)
					{
						return false;
					}
				}
			}
			return base.AreConstrainsValid(x, y);
		}
	}
}
