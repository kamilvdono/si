﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad2.Data
{
	class SkyscrapperEnvironment : CubeEnvironment
	{
		public Dictionary<SideEnum, int[]> Edges;
		
		public override void InitBoard(int n)
		{
			base.InitBoard(n);
			SetupEnvironment();
		}

		public void SetEdge(SideEnum side, int x, int value)
		{
			Edges[side][x] = value;
		}

		public override void ForwardEliminate()
		{
			foreach (var sideEnum in SideEnum.AllEnums)
			{
				for (int x = 0; x < Size; x++)
				{
					ForwardEliminateSide(sideEnum, x, Edges[sideEnum][x]);
				}
			}

			base.ForwardEliminate();
		}

		private void ForwardEliminateSide(SideEnum side, int x, int value)
		{
			if (value == 1)
			{
				var field = side.FirstField(Size, x);
				FillBoard(field, Size);
			}

			if (value == Size)
			{
				var field = side.FirstField(Size, x);
				Func<int, bool> some;
				if (side.Distancer == 0)
				{
					some = (a) => a < Size;
				}
				else
				{
					some = (a) => a >= 0;
				}

				int add = side.Distancer * -2 + 1;

				if (side.Direction == 1)
				{
					for (int i = field.Y, val = 1; some(i); i += add, val++)
					{
						FillBoard(new BoardField(field.X, i), val);
					}
				}
				else
				{
					for (int i = field.X, val = 1; some(i); i += add, val++)
					{
						FillBoard(new BoardField(i, field.Y), val);
					}
				}


			}
		}

		public override string GetInHTML()
		{
			StringBuilder sb = new StringBuilder();

			sb.Append($"<html>\r\n<body><h3>{Name}</h3>\r\n\t<table>");
			//Top
			sb.Append("<tr><td></td>");
			foreach (var i in Edges[SideEnum.TOP])
			{
				sb.Append("<td style=\"color: red; \">");
				sb.Append(i.ToString());
				sb.Append("</td>");
			}
			sb.Append("<td></td></tr>");

			for (int x = 0; x < Size; x++)
			{
				sb.Append("<tr>");

				//LEFT
				sb.Append("<td style=\"color: red; \">");
				sb.Append(Edges[SideEnum.LEFT][x].ToString());
				sb.Append("</td>");

				for (int y = 0; y < Size; y++)
				{
					sb.Append("<td>");
					sb.Append(Board[x,y].ToString());
					sb.Append("</td>");
				}

				//RIGHT
				sb.Append("<td style=\"color: red; \">");
				sb.Append(Edges[SideEnum.RIGHT][x].ToString());
				sb.Append("</td>");

				sb.Append("</tr>");
			}

			//Bottom
			sb.Append("<tr><td></td>");
			foreach (var i in Edges[SideEnum.BOTTOM])
			{
				sb.Append("<td style=\"color: red; \">");
				sb.Append(i.ToString());
				sb.Append("</td>");
			}
			sb.Append("<td></td></tr>");

			sb.Append("</table>\r\n</body>\r\n</html>");

			return sb.ToString();
		}

		private void SetupEnvironment()
		{
			Edges = new Dictionary<SideEnum, int[]>();
			foreach (var sideEnum in SideEnum.AllEnums)
			{
				Edges.Add( sideEnum, new int[Size] );
			}

			Board = new int[Size,Size];
		}

		public override CubeEnvironment Clone()
		{
			SkyscrapperEnvironment clone = new SkyscrapperEnvironment();
			clone.InitBoard(Size);
			clone.Edges = Edges;
			clone.Name = Name;

			for (int x = 0; x < Size; x++)
			{
				for (int y = 0; y < Size; y++)
				{
					clone.Board[x, y] = Board[x, y];
				}
			}
			clone.Heuristic = new List<BoardField>(Heuristic);
			clone.Domain = CloneDomain();
			clone.DomainChanges = CloneDomainChanges();
			return clone;
		}

		public override bool AreConstrainsValid(int x, int y)
		{
			//TOP
			var topEdge = Edges[SideEnum.TOP][y];
			if (topEdge != 0)
			{
				int visible = 0;
				int biggest = -1;
				for (int x2 = 0; x2 < Size; x2++)
				{
					if (x2 == 0 && Board[x2, y] == Size && topEdge != 1)
					{
						return false;
					}

					if (Board[x2, y] == 0)
					{
						visible = topEdge;
						break;
					}

					if (Board[x2, y] > biggest)
					{
						biggest = Board[x2, y];
						visible++;
					}

					if (visible > topEdge) return false;
				}

				if (visible != topEdge) return false;
			}

			//BOTTOM
			var bottomEdge = Edges[SideEnum.BOTTOM][y];
			if (bottomEdge != 0)
			{
				int visible = 0;
				int biggest = -1;
				for (int x2 = Size-1; x2 >= 0; x2--)
				{
					if (x2 == Size - 1 && Board[x2, y] == Size && bottomEdge != 1)
					{
						return false;
					}

					if (Board[x2, y] == 0)
					{
						visible = bottomEdge;
						break;
					}

					if (Board[x2, y] > biggest)
					{
						biggest = Board[x2, y];
						visible++;
					}

					if (visible > bottomEdge) return false;
				}

				if (visible != bottomEdge) return false;
			}

			//LEFT
			var leftEdge = Edges[SideEnum.LEFT][x];
			if (leftEdge != 0)
			{
				int visible = 0;
				int biggest = -1;
				for (int y2 = 0; y2 < Size; y2++)
				{
					if (y2 == 0 && Board[x, y2] == Size && leftEdge != 1)
					{
						return false;
					}

					if (Board[x, y2] == 0)
					{
						visible = leftEdge;
						break;
					}

					if (Board[x, y2] > biggest)
					{
						biggest = Board[x, y2];
						visible++;
					}

					if (visible > leftEdge) return false;
				}

				if (visible != leftEdge) return false;
			}

			//RIGHT
			var rightEdge = Edges[SideEnum.RIGHT][x];
			if (rightEdge != 0)
			{
				int visible = 0;
				int biggest = -1;
				for (int y2 = Size - 1; y2 >= 0; y2--)
				{
					if (y2 == Size - 1 && Board[x, y2] == Size && rightEdge != 1)
					{
						return false;
					}

					if (Board[x, y2] == 0)
					{
						visible = rightEdge;
						break;
					}

					if (Board[x, y2] > biggest)
					{
						biggest = Board[x, y2];
						visible++;
					}

					if (visible > rightEdge) return false;
				}

				if (visible != rightEdge) return false;
			}

			return base.AreConstrainsValid(x, y);
		}
	}
}
