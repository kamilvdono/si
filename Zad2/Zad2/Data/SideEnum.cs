﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad2.Data
{
	class SideEnum
	{
		public static readonly SideEnum TOP = new SideEnum('G', 0, 0, 0);
		public static readonly SideEnum BOTTOM = new SideEnum('D', 1, -1, 0);
		public static readonly SideEnum LEFT = new SideEnum('L', 0, 0, 1);
		public static readonly SideEnum RIGHT = new SideEnum('P', 1, -1, 1);

		public static readonly SideEnum[] AllEnums = {TOP, BOTTOM, LEFT, RIGHT};

		public readonly char CharacterSign;
		public readonly int Distancer;
		private readonly int Offset;
		public readonly int Direction;

		private SideEnum(char sign, int distancer, int offset, int direction)
		{
			CharacterSign = sign;
			Distancer = distancer;
			Offset = offset;
			Direction = direction;
		}

		public BoardField FirstField(int size, int xy)
		{
			int y = xy * (1 - Direction) + (size * Distancer + Offset) * (Direction);
			int x = (size * Distancer + Offset) * (1 - Direction) + xy * (Direction);
			return new BoardField(x, y);
		}
	}
}
