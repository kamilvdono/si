﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;

namespace Zad2.Data
{
	static class FutoshikiDataLoader
	{
		public static FutoshikiEnvironment LoadFutoshikiEnvironment(string filePath)
		{
			if (File.Exists(filePath) == false)
			{
				Console.WriteLine($"There is no file at path {filePath}");
				return null;
			}

			var fileText = File.ReadAllText(filePath);
			var splittedFile = fileText.Split(':').Select(s => s.Replace("START", "").Replace("REL", "").Trim()).ToArray();
			var n = int.Parse(splittedFile[0]);

			var board = splittedFile[1].Split('\n');
			if (board.Length != n)
			{
				Console.WriteLine($"N is not equal board size");
				return null;
			}
			var relations = splittedFile[2].Split('\n');

			FutoshikiEnvironment environment = new FutoshikiEnvironment();
			environment.InitBoard(n);

			for (var i = 0; i < board.Length; i++)
			{
				var boardLine = board[i];
				var lineFields = boardLine.Split(';');
				for (var index = 0; index < lineFields.Length; index++)
				{
					var lineField = lineFields[index].TrimEnd();
					if (lineField != "0")
					{
						environment.FillBoard(new BoardField(i, index), int.Parse(lineField));
					}
				}
			}

			for (int i = 0; i < relations.Length; i++)
			{
				var relation = relations[i].Split(';');
				var x0 = relation[0][0];
				var y0 = int.Parse(relation[0].Substring(1)) - 1;
				var x1 = relation[1][0];
				var y1 = int.Parse(relation[1].Substring(1)) - 1;
				environment.AddRelation(x0, y0, x1, y1);
			}

			environment.Valid();
			environment.Name = Path.GetFileName(filePath);

			return environment;
		}
	}
}
