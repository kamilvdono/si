﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Zad2.Data;

namespace Zad2
{
	class Program
	{
		private const bool debug = false;

		static void Main(string[] args)
		{
			var files = System.IO.Directory.GetFiles("Dane")
				.Where(f => f.Contains("futo"))
				.ToList();

			//SolveBucket(files, false, debug);
			SolveBucket(files, true, debug);

			Console.ReadLine();
		}

		private static void SolveBucket(System.Collections.Generic.List<string> files, bool forwardChecking, bool debug)
		{
			if (Directory.Exists("Dane/Out") == false)
			{
				Directory.CreateDirectory("Dane/Out");
			}
			else
			{
				Directory.Move("Dane/Out", $"Dane/Out_{DateTime.Now.Ticks}");
				Directory.CreateDirectory("Dane/Out");
			}

			Stopwatch loadAndPrepare = new Stopwatch();
			Stopwatch solve = new Stopwatch();
			Stopwatch dump = new Stopwatch();

			foreach (var file in files)
			{
				loadAndPrepare.Start();
				var loaded = DataLoader.Load(file);
				if(forwardChecking) loaded.ForwardEliminate();
				loaded.CalcHeuristic();
				loadAndPrepare.Stop();

				solve.Start();
				var s = (forwardChecking ? CubeSolver.SolveForward(loaded, debug) : CubeSolver.Solve(loaded, debug));
				solve.Stop();

				var solved = s.Item1;

				dump.Start();
				File.WriteAllText("Dane/Out/" + Path.GetFileNameWithoutExtension(file) + "_"+ forwardChecking + ".html", loaded.GetInHTML() + (s.Item2 ? " \nSOLVED\n " : " \n CAN NOT BE SOLVED\n ") + solved.GetInHTML());
				dump.Stop();

				Console.WriteLine($"DONE: {file}. With forward checking? {forwardChecking}. Load {loadAndPrepare.Elapsed.TotalMilliseconds}ms, solving {solve.Elapsed.TotalMilliseconds}ms, dump {dump.Elapsed.TotalMilliseconds}ms ");
			}

			JoinAllHTML(10);

			Console.WriteLine($"Loaded in {loadAndPrepare.Elapsed.TotalMilliseconds}ms and solved in {solve.Elapsed.TotalMilliseconds}ms");
		}

		private static void JoinAllHTML(int columns = 4)
		{
			var files = System.IO.Directory.GetFiles("Dane/Out")
				.ToList();

			StringBuilder sb = new StringBuilder();
			sb.Append("<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\"><tr>");

			for (int i = 0; i < files.Count; i++)
			{
				if (i % columns == 0)
				{
					sb.Append("</tr><tr>");
				}

				sb.Append("<td>");
				sb.Append(File.ReadAllText(files[i]));
				sb.Append("</td>");
			}
			sb.Append("</tr></table>");

			File.WriteAllText("Dane/Out/Summary.html", sb.ToString());
		}

	}
}
