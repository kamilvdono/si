﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad2
{
	class Utils
	{
		
	}

	public struct BoardField
	{
		public int X;
		public int Y;

		public BoardField(int x, int y)
		{
			X = x;
			Y = y;
		}

		public bool Equals(int x, int y)
		{
			return X == x && Y == y;
		}
	}
}
